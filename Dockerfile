# Do we want to pin the version of ansible-runner?  best practice is to lock to
# a specific version, but not sure if that's really what we want, so ignoring
# it for now
# hadolint ignore=DL3007
FROM ansible/ansible-runner:latest

# Add custom python3 packages to the image for windows team
COPY requirements.txt /opt/requirements.txt
COPY krb5.conf /etc/krb5.conf

RUN yum install -y krb5-workstation \
    && yum install -y krb5-devel \
    && yum install -y krb5-libs \
    && yum clean all

RUN pip3 install --no-cache-dir -r /opt/requirements.txt
